# Linee guida tesi

0. Consegna dei capitoli
    - Ogni capitolo va consegnato una sola volta, uno per volta
    - Prima di consegnarlo
        - **Fare un controllo della grammatica** ed eventuali errori di battitura (per tesi in inglese Gramamrly � di grande aiuto)
        - Metterlo "nel cassetto" 2-3 giorni e rileggerlo
0. Linguaggio
    - Evitare sempre commenti non oggettivi/superficiali/informali (o sono supportati da citazioni o non si mettono). Ad esempio: banale, semplice, facile, difficile, molto, troppo, poco, impossibile
    - Concisione: Non scrivere frasi pi� lunghe di *due* righe
        - Esempio No: L'idea di traiettoria si delinea nell'abilit� di catturare gli spostamenti di un oggetto
        - Esempio S�: La definizione di traiettoria �: "testo quotato" [citazione]
    - **Evitare** l'abuso di **grassetto** e *corsivo*
0. Inglesismi
    - Se le tesi � in Italiano � bene scrivere in Italiano e limitare l'uso dell'inglese il pi� possibile (e.g., "Trajectory Mining" -> "Mining di traiettorie")
        - Ovviamente non tutto pu� essere tradotto in Italiano (e.g., clustering, mining, deploy, etc.)
    - Usa un termine inglese se questo si ripete ma spiegalo una sola volta.
    - Un acronimo in inglese rimane in inglese: GPS (Global Positioning System) e non si usa il plurale
    - La prima volta in assoluto che il termine compare scrivilo in corsivo. Esempio "I *layer* (livelli) di un GIS (*Geographic Information System*)"
0. Struttura
    - Convenzione nomi
        - Titolo: 
            - Prima lettera maiuscola in tutti i nomi, pronomi, aggettivi, verbi, avverbi
            - Tutto maiuscolo acronimi
            - In minuscolo articoli e congiungzioni 
        - Sezioni: tutto minuscolo eccetto la prima lettera e acronimi (e.g., "Questa � una sessione OLAP")
    - Abstract (sommario): singolo paragrafo tra 150 e 250 parole
    - Usare il package cleveref e `\Cref{}` (e non `\ref{}`) di Latex per fare riferimento a label di capitoli/sezioni/sottosezioni/immagini
    - Quando si descrive un argomento � bene averlo gi� introdotto. Se non indispensabili, evitare le *forword reference* (riferimento a capitoli/sezioni/sottosezioni che appaiono dopo essere referenziati)
    - Non creare sezioni con singola sottosezione
    - Non creare (sotto)sezioni lunghe meno di una pagina
    - Evita di disperdere le informazioni: definizione, dettagli e implicazioni stanno nello stesso paragrafo
    - Esiste differenza tra `.` e `. a capo`: `. a capo` si usa per cambiare discorso
0. Citazioni e copia/incolla
    - Non citare Wikipedia (su https://scholar.google.it/ esiste un oceano di letteratura)
        - Se necessario, sfruttare Wikipedia per trovare le citazioni a paper/libri
    - Non copiare testi troppo lunghi, � meglio riassumerli ed elaborarli
    - Non copiare traduzioni troppo lunghe, � meglio riassumerle ed elaborarle
    - Esplicita la parte di testo copiata inserendola tra virgolette e aggiungi la citazione finale
        - Esempio: "Aggiungere inglesismi random non improva le vostre skills" [[Questa � una cit]](http://www.lercio.it/ricerca-aggiungere-inglesismi-random-non-improva-le-vostre-skills/)
    - La citazione precede il `.`: Testo citato [citazione]. (e non: Testo citato. [citazione]
0. Figure:
    - Se una figura � copiata/rielaborata indica la sorgente con apposita citazione
    - Utilizzare figure in formato vettoriale (.pdf, .svg)
    - In caso di screenshot, attenzione alle dimensioni (in MB) della figura
0. Punteggiatura, elenchi (e coerenza)
    - No punti nei titoli
    - Non `E'` ma `�` (� = Alt + 0200)  
    - In Latex si virgolette si fanno \`\`cos�'' (\` = Alt + 96) e non "cos�"
    - Elenchi puntati iniziano con Maiuscola, finiscono con `;` o con `.` (l'importante � usare la stessa convenzione). Di solito si usa `;` per terminare frasi di senso non compiuto, e `.` per terminare frasi di senso compiuto
    - No `...`, s� "etc." 
0. Link di riferimento per elaborato tesi https://corsi.unibo.it/magistrale/IngegneriaScienzeInformatiche/volume-pdf-e-deposito-online-dellelaborato